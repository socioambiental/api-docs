Documentação de API SisArp (versão 01) 
=========================================

# Objetivo #

A API tem como objetivo principal abrir para consumo os dados que estão armazenados sob o domínio do SisArp (Sistemas de Áreas Protegidas).


# Endpoint #

A versão 01 da API do SisArp está disposta sob o seguinte endpoint https://api.socioambiental.org/sisarp/v1/

## Enpoints públicos (abertos) e privados (fechados) ##

Nem todos os recursos estão dispostos de forma pública, para alguns conjuntos de dados é necessário uma autenticação do tipo *http basic authentication*.

Caso haja a necessidade de acesso a algum recurso privado (fechado) entre em contato com a equipe mantenedora do ISA para que possa ser disponibilizado um usuário e hash para tal acesso.


# Formatos de saída #

Os formatos esperados de saída para um recurso da API são JSON ou XML. Um exemplo de consumo de recursos poderia ser a seguinte requisição *https://api.socioambiental.org/sisarp/v1/noticia.json* 

No caso acima descrito, a requisição irá retornar a primeira página de notícias no formato JSON de saída. 


# Descrição dos Recursos disponíveis na API#

Diversos temas que estão sistematizados no SisArp estão expostos nesta API para possibilitar a integração de dados entre sistemas, sites e outros softwares. Abaixo serão descritos cada um destes recursos.

## Arp (Listagem de Terras Indígenas, Unidades de Conservação ou Quilombos) ##

Este recurso retorna listagens de áreas protegidas. 

O endpoint é *https://api.socioambiental.org/sisarp/v1/arp*

Este conjunto de dados necessita de autenticação para acesso.

### Formato do recurso ###
>*[arp_id]* - ID da área protegida (integer)

>*[categoria]* => Categoria (sigla) da Área Protegida (string)

>*[descricao_categoria]* => Descrição categoria da área protegida (string)

>*[nome_arp]* => Nome da área protegida (string)

>*[area_oficial]* => Área oficial da área protegida (integer)

>*[area_isa]* => Área calculada (ISA) da área protegida (integer)

>*[jurisdicao_legal]* => Jurisdição legal de onde se encontra a área protegida (string)

>*[populacao_arp]* => População (quando houver) da área protegida (string)


### Filtros possíveis ###

É possível utilizar um filtro apenas ou uma combinação (operador AND) de filtros os tipos de filtragem são:

__ambito__ (string) => Os valores possíveis para este filtro são "TI" para terras indígenas, "UC" para unidades de conservação e "QUI" para quilombos.

**jurisdicao** (string) => Os valores possíveis para este filtro são "Amazônia Legal",  "Domínio Mata Atlântica" e "Outros".

**bacia** (integer) => O valor esperado para filtragem é o ID da bacia (vide SisArp).

**bioma** (integer) => O valor esperado para filtragem é o ID do bioma (vide SisArp).

**fitofisionomia** (integer) => O valor esperado para filtragem é o ID da fitofisionomia (vide SisArp).

**categoria** (string) => O valor esperado para filtragem é a sigla da Categoria (vide SisArp/SNUC).

**municipio** (integer) => O valor esperado para filtragem é o código IBGE do município (vide SisArp/IBGE). *Para este recurso é possível buscar múltiplos municípios encadeando os códigos com "," por exemplo (cód ibge01, cód ibge02, ...)*

**uf** (string) => O valor esperado para filtragem é a UF de um estado (vide SisArp/IBGE).*Para este recurso é possível buscar múltiplas ufs encadeando as ufs com "," por exemplo (UF 01, UF 02, ...)*

**situacaojuridica** (string) => O valor esperado para filtragem são as situações jurídicas aplicadas a UCs, TIs ou Quilombos (vide SisArp).

=======================================================================================================================================================================================================================

## Município (Listagem de municípios) ##

Este recurso retorna listagens de informações sobre municípios.

O endpoint é *https://api.socioambiental.org/sisarp/v1/municipio*

Este conjunto de dados necessita de autenticação para acesso.

### Formato do recurso ###
>*[nome_municipio]* - Nome do município (string)

>*[code_ibge]* => Código IBGE do município (integer)

>*[uf]* => UF na qual o município está localizado (string)


### Filtros possíveis ###

É possível utilizar um filtro apenas ou uma combinação (operador AND) de filtros os tipos de filtragem são:

**municipio** (integer) => O valor esperado para filtragem é o código IBGE do município (vide SisArp/IBGE). *Para este recurso é possível buscar múltiplos municípios encadeando os códigos com "," por exemplo (cód ibge01, cód ibge02, ...)*

**uf** (string) => O valor esperado para filtragem é a UF de um estado (vide SisArp/IBGE).*Para este recurso é possível buscar múltiplas ufs encadeando as ufs com "," por exemplo (UF 01, UF 02, ...)*

=======================================================================================================================================================================================================================

## Notícias (Listagem de notícias) ##

Este recurso retorna listagens de notícias que estão sistematizadas no SisArp, tais notícias são inseridas pela equipe do monitoramento e pela equipe de Documentação do ISA. 

O endpoint é https://api.socioambiental.org/sisarp/v1/noticia  

Este conjunto de dados necessita de autenticação para acesso.

Para melhoria de performance e evitar um excesso de carga sobre a API a listagem de notícias é limitada a 500 itens por chamada.

### Formato do recurso ###
>*[noticia_id]* - ID da notícia (integer)

>*[titulo]* => Título da notícia (string 255 posições)

>*[fonte_noticia]* => Descrição da fonte que veiculou a notícia (string 255 posições)

>*[dtpublicacao_ntc]* => Data de publicação da notícia no formato yyyy-mm-dd (date)

>*[data_publicacao]* => Data de publicação da notícia no formato dd-mm-yyyy (date)

Vale salientar que o recurso além de retornar os dados descritos acima, há ainda meta informações relativas ao retorno de recurso, sendo elas:

>*[total]* - Total de registros que a listagem possui.

>*[total_pages]* - Total de páginas (valor de referência 500 itens) que a listagem possui.

### Filtros possíveis ###

É possível utilizar um filtro apenas ou uma combinação (operador AND) de filtros os tipos de filtragem são:

__ambito__ (string) => Os valores possíveis para este filtro são "TI" para terras indígenas, "UC" para unidades de conservação e "QUI" para quilombos.

**id** (integer) => ID de uma área protegida (UC, TI ou Quilombo - Vide SisArp).

### Paginação ###

É possível utilizar um parâmetro de paginação apenas ou uma combinação (operador AND) de parâmetros os valores possíveis são:

__page__ (integer) => Valor numérico da página que se quer.

**limit** (integer) => Limite de registros que se necessita de retorno.

=======================================================================================================================================================================================================================

## Detalhe de Notícias ##

Este recurso retorna o detalhamento de notícias que estão sistematizadas no SisArp, tais notícias são inseridas pela equipe do monitoramento e pela equipe de Documentação do ISA. 

O endpoint é https://api.socioambiental.org/sisarp/v1/detalhe_noticia

Este conjunto de dados necessita de autenticação para acesso.


### Formato do recurso ###
>*[noticia_id]* - ID da notícia (integer)

>*[titulo]* => Título da notícia (string 255 posições)

>*[fonte_noticia]* => Descrição da fonte que veiculou a notícia (string 255 posições)

>*[data_publicacao]* => Data de publicação da notícia no formato dd-mm-yyyy (date)

>*[corpo_noticia]* => Corpo da notícia (string de 0 a N posições).

>*[area_protegida]* => Listagem de áreas protegidas associadas a notíca

>>*[areaprotegida_id]* (integer) => ID da área protegida

>>*[tipo]* (string) => Tipo da área protegida (TI, UC ou QUI)

>>*[nome_area_protegida]* (string) => Nome da área protegida

>*[palavra_chave]* => Listagem de palavras chave associadas a notíca

>>*[palavra_chave_id]* (integer) => ID da palavra chave

>>*[palavra_chave]* (string) => Descrição da palavra chave

>*[anexo]* => Listagem de arquivos anexos associados a notícia

>>*[anexo_id]* (integer) => ID do arquivo anexo

>>*[descricao]* (string) => Descrição do arquivo anexo

>>*[nome_arquivo]* (string) => Nome do arquivo anexo

>>*[link]* (string) => Link público para anexo da notícia


### Filtros possíveis ###

É possível utilizar um filtro apenas ou uma combinação (operador AND) de filtros os tipos de filtragem são:

**noticia_id** (integer) => ID de uma notícia, este parâmetro é obrigatório. 

=======================================================================================================================================================================================================================

## Obra (Listagem e detalhamento de informações sobre obras) ##

Este recurso retorna informações relativas as obras cadastradas no SisArp.

O endpoint é https://api.socioambiental.org/sisarp/v1/obra

Este conjunto de dados necessita de autenticação para acesso.

### Formato de retorno do recurso ###
>*[id]* (integer) => ID da área protegida

>*[obra]* (array) => Listagem contendo informações sobre a obra

>>>*[info_geral]* => Informações gerais sobre a obra

>>>>*[id]* (integer) => ID da obra

>>>>*[sigla]* (string) => Sigla da obra

>>>>*[nome]* (string) => Nome da obra

>>>>*[outro_nome]* (string) => Outros nomes associados a obra

>>>>*[setor]* (string) => Setor da obra (Energia, transporte, comunicação, etc)

>>>>*[categoria]* (string) => Categoria da obra (Estrada, ponte, UHE, etc)

>>>>*[tipo_obra]* (string) => Tipo da obra (Construção, reforma, melhoria, etc)

>>>>*[id_obra_pai]* (integer) => ID da obra pai (quando houver)

>>>>*[tipo_relacao_obra_pai]* (string) => Tipo de relação com obra pai (Trecho, componente, etc)

>>>>*[justificativa]* (string) => Justificativa para a realização da obra

>>>>*[estagio_andamento]* (string) => Estágio de andamento da obra

>>>>*[ano_estagio_andamento]* (string) => Ano relativo ao estágio de andamento da obra

>>>>*[ano_inicio_previsto]* (integer) => Ano de início previsto para a obra

>>>>*[ano_inicio_efetivo]* (integer) => Ano de início efetivo da obra

>>>>*[composicao_custo]* (string) => Composição de custos da obra

>>>>*[jurisdicao]* (string) => Jurisdição na qual a obra está associada (Federal, Estadual, etc)

>>>>*[responsavel_obra]* (string) => Responsável pela obra (Privado, público ou Público/Privado)

>>>>*[possui_aia]* (string) => Define se a obra possui AIA

>>>>*[possui_avaliacao_socioambiental]* (string) => Define se a obra possui avaliação socioambiental

>>>>*[possui_plano_salvaguarda]* (string) => Define se a obra possui plano de salvaguarda

>>>>*[programa_socioambiental]* (string) => Define se a obra participa de algum programa socioambiental

>>>>*[valor_compensacao_ambiental]* (numeric) => Valor da compensação ambiental

>>>>*[fonte_informacao_obra]* (string) => Fonte de informação sobre as informações gerais de uma obra

>>>*[arp]* => Listagem de áreas protegidas associadas a obra

>>>>*[id_arp]* (integer) => ID da área protegida

>>>>*[tipo_arp]* (string) => Tipo da área protegida (TI, UC ou QUI)

>>>>*[nome_arp]* (string) => Nome da área protegida

>>>*[prazo_custo]* => Listagem de informações sobre prazos e custos de uma obra

>>>>*[data_prazo]* (date) => Data do prazo de entrega da obra

>>>>*[valor]* (numeric) => Valor total da obra

>>>>*[moeda]* (string) => Moeda na qual o valor está referenciado

>>>>*[data_revisao]* (date) => Data em que foi feita a revisão no prazo/custo

>>>>*[fonte_informacao_prazo]* (string) => Fonte da informação sobra prazo/custo da obra


>>>*[composicao_societaria]* => Listagem da composição societária associada a obra

>>>>*[instituicao_id]* (integer) => ID da instituição

>>>>*[nome_instituicao]* (string) => Nome da instituição que compões a composição societária da obra

>>>>*[porcentagem]* (numeric) => Porcentagem da participação da instituição na composição societária

>>>*[localizacao]* => Listagem de informações sobre localização da obra

>>>>*[localizacao_id]* (integer) => ID da localização (Se município o ID é o código do IBGE, se estado é o ID SisArp do Estado e se for país é o ID SisArp do País)

>>>>*[tipo]* (string) => Tipo da localização (Município, estado ou país)

>>>>*[pais]* (string) => Descritivo do país

>>>>*[estado]* (string) => Descritivo do Estado

>>>>*[municipio]* (string) => Nome do município

>>>*[bacia]* => Listagem de informações sobre bacias associadas a obra

>>>>*[bacia_id]* (integer) => ID da bacia

>>>>*[bacia]* (string) => Nome da bacia

>>>*[financiador]* => Listagem de informações sobre financiadores associados a obra

>>>>*[financiador_id]* (integer) => ID do Financiador

>>>>*[financiador]* (string) => Nome do financiador

>>>*[construtor]* => Listagem de informações sobre construtores associados a obra

>>>>*[construtor_id]* (integer) => ID do construtor

>>>>*[construtor]* (string) => Nome do contrutor da obra

>>>*[planejamento]* => Listagem de informações sobre planejamento setorial, nacional ou regional associados a obra

>>>>*[descricao_planejamento]* (string) => Descrição do planejamento setorial, nacional ou regional

>>>>*[fonte_informacao]* (string) => Fonte da informação sobre planejamento

>>>*[audiencia_publica]* => Listagem de informações sobre audiências públicas associadas a obra

>>>>*[data_audiencia]* (date) => Data da audiência pública

>>>>*[local_audiencia]* (string) => Local da audiência pública

>>>>*[arquivo_ata]* (link) => Link para arquivo que contenha a ata da audiência pública

### Filtros possíveis ###

É possível utilizar um filtro apenas ou uma combinação (operador AND) de filtros os tipos de filtragem são:

>**ambito** (string) => Os valores possíveis para este filtro são "TI" para terras indígenas, "UC" para unidades de conservação e "QUI" para quilombos.

>**jurisdicao** (string) => Os valores possíveis para este filtro são "Amazônia Legal",  "Domínio Mata Atlântica" e "Outros".

>**bacia** (ingeger) => O valor esperado para filtragem é o ID da bacia (vide SisArp).

>**bioma** (integer) => O valor esperado para filtragem é o ID do bioma (vide SisArp).

>**fitofisionomia** (integer) => O valor esperado para filtragem é o ID da fitofisionomia (vide SisArp).

>**categoria** (string) => O valor esperado para filtragem é a sigla da Categoria (vide SisArp/SNUC).

>**municipio** (integer) => O valor esperado para filtragem é o código IBGE do município (vide SisArp/IBGE). *Para este recurso é possível buscar múltiplos municípios encadeando os códigos com "," por exemplo (<cód ibge01>, <cód ibge02>, ...)*

>**uf** (string) => O valor esperado para filtragem é a UF de um estado (vide SisArp/IBGE).*Para este recurso é possível buscar múltiplas ufs encadeando as ufs com "," por exemplo (<UF 01>, <UF 02>, ...)*

>**situacaojuridica** (string) => O valor esperado para filtragem são as situações jurídicas aplicadas a UCs, TIs ou Quilombos (vide SisArp).
